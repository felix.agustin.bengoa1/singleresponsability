using SingleResponsabilityTp.Model;

namespace SingleResponsabilityTp
{
    public partial class Form1 : Form
    {
        private Empleado ?empleado;
        private EnviarInforme enviarInforme;
        private CalculadorSalario calculadorSalario;

        public Form1()
        {
            InitializeComponent();
            calculadorSalario = new CalculadorSalario();
            enviarInforme = new EnviarInforme();
        }

        private void BtnCalculateSalary_Click(object sender, EventArgs e)
        {
            try
            {
                string name = TxtName.Text;
                string posicion = TxtPosition.Text;
                double hourlySalary = Convert.ToDouble(TxtHourlySalary.Text);
                int diasTrabajados = Convert.ToInt32(TxtDiasTrabajados.Text);

                empleado = new Empleado(name, posicion, hourlySalary);
                double sueldoMensual = calculadorSalario.CalcularSueldoMensual(empleado, diasTrabajados);

                LblSalary.Text = $"Sueldo mensual: {sueldoMensual:C}";
            }
            catch (FormatException)
            {
                MessageBox.Show("Por favor ingrese valores v�lidos.");
            }
        }

        private void BtnEnviarInforme_Click(object sender, EventArgs e)
        {
            if (empleado == null)
            {
                MessageBox.Show("Por favor calcule el sueldo primero.");
                return;
            }
            if (string.IsNullOrWhiteSpace(TxtEmail.Text))
            {
                MessageBox.Show("por favor ingresar correo electronico");
                return;
            }

            string email = TxtEmail.Text;
            string subject = $"Informe de Sueldo de {empleado.Name}";
            string message = LblSalary.Text;

            enviarInforme.informe(email, subject, message);
            limpiarCampos();
        }

        private void limpiarCampos()
        {
            TxtEmail.Clear();
            TxtDiasTrabajados.Clear();
            TxtHourlySalary.Clear();
            TxtName.Clear();
            TxtPosition.Clear();
            LblSalary.Text = string.Empty;
            empleado = null;
        }
    }
}
