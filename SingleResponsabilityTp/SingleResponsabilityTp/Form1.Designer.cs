﻿namespace SingleResponsabilityTp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            LblName = new Label();
            LblPosition = new Label();
            LblHourlySalary = new Label();
            LblEmail = new Label();
            BtnCalculateSalary = new Button();
            BtnEnviarInforme = new Button();
            LblDiasTrabajados = new Label();
            TxtName = new TextBox();
            TxtPosition = new TextBox();
            TxtHourlySalary = new TextBox();
            TxtEmail = new TextBox();
            TxtDiasTrabajados = new TextBox();
            LblSalary = new Label();
            SuspendLayout();
            // 
            // LblName
            // 
            LblName.AutoSize = true;
            LblName.Location = new Point(232, 57);
            LblName.Name = "LblName";
            LblName.Size = new Size(64, 20);
            LblName.TabIndex = 0;
            LblName.Text = "Nombre";
            // 
            // LblPosition
            // 
            LblPosition.AutoSize = true;
            LblPosition.Location = new Point(232, 105);
            LblPosition.Name = "LblPosition";
            LblPosition.Size = new Size(63, 20);
            LblPosition.TabIndex = 1;
            LblPosition.Text = "Posicion";
            // 
            // LblHourlySalary
            // 
            LblHourlySalary.AutoSize = true;
            LblHourlySalary.Location = new Point(192, 151);
            LblHourlySalary.Name = "LblHourlySalary";
            LblHourlySalary.Size = new Size(103, 20);
            LblHourlySalary.TabIndex = 2;
            LblHourlySalary.Text = "Pago por hora";
            // 
            // LblEmail
            // 
            LblEmail.AutoSize = true;
            LblEmail.Location = new Point(249, 238);
            LblEmail.Name = "LblEmail";
            LblEmail.Size = new Size(46, 20);
            LblEmail.TabIndex = 3;
            LblEmail.Text = "Email";
            // 
            // BtnCalculateSalary
            // 
            BtnCalculateSalary.Location = new Point(314, 315);
            BtnCalculateSalary.Name = "BtnCalculateSalary";
            BtnCalculateSalary.Size = new Size(129, 29);
            BtnCalculateSalary.TabIndex = 4;
            BtnCalculateSalary.Text = "Calcular Salario";
            BtnCalculateSalary.UseVisualStyleBackColor = true;
            BtnCalculateSalary.Click += BtnCalculateSalary_Click;
            // 
            // BtnEnviarInforme
            // 
            BtnEnviarInforme.Location = new Point(314, 364);
            BtnEnviarInforme.Name = "BtnEnviarInforme";
            BtnEnviarInforme.Size = new Size(125, 29);
            BtnEnviarInforme.TabIndex = 5;
            BtnEnviarInforme.Text = "Enviar Informe";
            BtnEnviarInforme.UseVisualStyleBackColor = true;
            BtnEnviarInforme.Click += BtnEnviarInforme_Click;
            // 
            // LblDiasTrabajados
            // 
            LblDiasTrabajados.AutoSize = true;
            LblDiasTrabajados.Location = new Point(181, 196);
            LblDiasTrabajados.Name = "LblDiasTrabajados";
            LblDiasTrabajados.Size = new Size(115, 20);
            LblDiasTrabajados.TabIndex = 6;
            LblDiasTrabajados.Text = "Dias Trabajados";
            // 
            // TxtName
            // 
            TxtName.Location = new Point(314, 54);
            TxtName.Name = "TxtName";
            TxtName.Size = new Size(125, 27);
            TxtName.TabIndex = 7;
            // 
            // TxtPosition
            // 
            TxtPosition.Location = new Point(314, 98);
            TxtPosition.Name = "TxtPosition";
            TxtPosition.Size = new Size(125, 27);
            TxtPosition.TabIndex = 8;
            // 
            // TxtHourlySalary
            // 
            TxtHourlySalary.Location = new Point(314, 144);
            TxtHourlySalary.Name = "TxtHourlySalary";
            TxtHourlySalary.Size = new Size(125, 27);
            TxtHourlySalary.TabIndex = 9;
            // 
            // TxtEmail
            // 
            TxtEmail.Location = new Point(314, 231);
            TxtEmail.Name = "TxtEmail";
            TxtEmail.Size = new Size(125, 27);
            TxtEmail.TabIndex = 10;
            // 
            // TxtDiasTrabajados
            // 
            TxtDiasTrabajados.Location = new Point(314, 189);
            TxtDiasTrabajados.Name = "TxtDiasTrabajados";
            TxtDiasTrabajados.Size = new Size(125, 27);
            TxtDiasTrabajados.TabIndex = 11;
            // 
            // LblSalary
            // 
            LblSalary.AutoSize = true;
            LblSalary.Location = new Point(314, 279);
            LblSalary.Name = "LblSalary";
            LblSalary.Size = new Size(129, 20);
            LblSalary.TabIndex = 12;
            LblSalary.Text = "--------------------";
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(LblSalary);
            Controls.Add(TxtDiasTrabajados);
            Controls.Add(TxtEmail);
            Controls.Add(TxtHourlySalary);
            Controls.Add(TxtPosition);
            Controls.Add(TxtName);
            Controls.Add(LblDiasTrabajados);
            Controls.Add(BtnEnviarInforme);
            Controls.Add(BtnCalculateSalary);
            Controls.Add(LblEmail);
            Controls.Add(LblHourlySalary);
            Controls.Add(LblPosition);
            Controls.Add(LblName);
            Name = "Form1";
            Text = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label LblName;
        private Label LblPosition;
        private Label LblHourlySalary;
        private Label LblEmail;
        private Button BtnCalculateSalary;
        private Button BtnEnviarInforme;
        private Label LblDiasTrabajados;
        private TextBox TxtName;
        private TextBox TxtPosition;
        private TextBox TxtHourlySalary;
        private TextBox TxtEmail;
        private TextBox TxtDiasTrabajados;
        private Label LblSalary;
    }
}
