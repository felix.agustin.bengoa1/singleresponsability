﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleResponsabilityTp.Model
{
    public class CalculadorSalario
    {
        public double CalcularSueldoMensual(Empleado empleado, int diasTrabajados) 
        {
            const int horasPorDia = 8;
            return diasTrabajados * horasPorDia * empleado.HourlySalary;
        }
    }
}
