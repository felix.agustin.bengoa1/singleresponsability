﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleResponsabilityTp.Model
{
    public class EnviarInforme
    {
        public void informe(string email, string subject, string message)
        {
            MessageBox.Show($"Email enviado a {email}\n Asunto: {subject}\n Mensaje: {message}\n");
        }
    }
}
