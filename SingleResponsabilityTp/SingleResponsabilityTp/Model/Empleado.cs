﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleResponsabilityTp.Model
{
    public class Empleado
    {
        public string Name {  get; set; }
        public string Position {  get; set; }
        public double HourlySalary {  get; set; }

        public Empleado(string name, string position, double hourlySalary) 
        {
            Name = name;
            Position = position;
            HourlySalary = hourlySalary;
        }
    }
}
